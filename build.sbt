import play.Project._

name := "hello-play-java"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaEbean, jdbc,
  "org.webjars" %% "webjars-play" % "2.2.2",
  "org.webjars" % "bootstrap" % "3.3.1")

playJavaSettings
