// @SOURCE:/home/gdmc/Documents/play/play2_demo/conf/routes
// @HASH:77b45a56784b4b046236846060a120bc9a80834a
// @DATE:Sun Jan 22 23:52:22 VET 2017

package controllers;

public class routes {
public static final controllers.ReverseWebJarAssets WebJarAssets = new controllers.ReverseWebJarAssets();
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();
public static final controllers.ReverseApplication Application = new controllers.ReverseApplication();
public static class javascript {
public static final controllers.javascript.ReverseWebJarAssets WebJarAssets = new controllers.javascript.ReverseWebJarAssets();
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();
public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication();
}
public static class ref {
public static final controllers.ref.ReverseWebJarAssets WebJarAssets = new controllers.ref.ReverseWebJarAssets();
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();
public static final controllers.ref.ReverseApplication Application = new controllers.ref.ReverseApplication();
}
}
          