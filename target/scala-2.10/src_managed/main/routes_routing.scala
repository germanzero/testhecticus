// @SOURCE:/home/gdmc/Documents/play/play2_demo/conf/routes
// @HASH:77b45a56784b4b046236846060a120bc9a80834a
// @DATE:Sun Jan 22 23:52:22 VET 2017


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:5
private[this] lazy val controllers_Application_index0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:8
private[this] lazy val controllers_Assets_at1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
        

// @LINE:9
private[this] lazy val controllers_WebJarAssets_at2 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("webjars/"),DynamicPart("file", """.+""",false))))
        

// @LINE:12
private[this] lazy val controllers_Application_listarPersonas3 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("personas"))))
        

// @LINE:13
private[this] lazy val controllers_Application_nuevaPersona4 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("nuevaPersona"))))
        

// @LINE:14
private[this] lazy val controllers_Application_eliminarPersona5 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("eliminarPersona/"),DynamicPart("Id", """[^/]+""",true))))
        

// @LINE:16
private[this] lazy val controllers_Application_getPersona6 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("persona/"),DynamicPart("Id", """[^/]+""",true))))
        

// @LINE:17
private[this] lazy val controllers_Application_deletePersona7 = Route("DELETE", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("persona/"),DynamicPart("Id", """[^/]+""",true))))
        

// @LINE:18
private[this] lazy val controllers_Application_createPersona8 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("persona/"))))
        

// @LINE:19
private[this] lazy val controllers_Application_updatePersona9 = Route("PUT", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("persona/"),DynamicPart("Id", """[^/]+""",true))))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """webjars/$file<.+>""","""controllers.WebJarAssets.at(file:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """personas""","""controllers.Application.listarPersonas()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """nuevaPersona""","""controllers.Application.nuevaPersona()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """eliminarPersona/$Id<[^/]+>""","""controllers.Application.eliminarPersona(Id:Int)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """persona/$Id<[^/]+>""","""controllers.Application.getPersona(Id:Int)"""),("""DELETE""", prefix + (if(prefix.endsWith("/")) "" else "/") + """persona/$Id<[^/]+>""","""controllers.Application.deletePersona(Id:Int)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """persona/""","""controllers.Application.createPersona()"""),("""PUT""", prefix + (if(prefix.endsWith("/")) "" else "/") + """persona/$Id<[^/]+>""","""controllers.Application.updatePersona(Id:Int)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:5
case controllers_Application_index0(params) => {
   call { 
        invokeHandler(controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Nil,"GET", """""", Routes.prefix + """"""))
   }
}
        

// @LINE:8
case controllers_Assets_at1(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        

// @LINE:9
case controllers_WebJarAssets_at2(params) => {
   call(params.fromPath[String]("file", None)) { (file) =>
        invokeHandler(controllers.WebJarAssets.at(file), HandlerDef(this, "controllers.WebJarAssets", "at", Seq(classOf[String]),"GET", """""", Routes.prefix + """webjars/$file<.+>"""))
   }
}
        

// @LINE:12
case controllers_Application_listarPersonas3(params) => {
   call { 
        invokeHandler(controllers.Application.listarPersonas(), HandlerDef(this, "controllers.Application", "listarPersonas", Nil,"GET", """""", Routes.prefix + """personas"""))
   }
}
        

// @LINE:13
case controllers_Application_nuevaPersona4(params) => {
   call { 
        invokeHandler(controllers.Application.nuevaPersona(), HandlerDef(this, "controllers.Application", "nuevaPersona", Nil,"GET", """""", Routes.prefix + """nuevaPersona"""))
   }
}
        

// @LINE:14
case controllers_Application_eliminarPersona5(params) => {
   call(params.fromPath[Int]("Id", None)) { (Id) =>
        invokeHandler(controllers.Application.eliminarPersona(Id), HandlerDef(this, "controllers.Application", "eliminarPersona", Seq(classOf[Int]),"GET", """""", Routes.prefix + """eliminarPersona/$Id<[^/]+>"""))
   }
}
        

// @LINE:16
case controllers_Application_getPersona6(params) => {
   call(params.fromPath[Int]("Id", None)) { (Id) =>
        invokeHandler(controllers.Application.getPersona(Id), HandlerDef(this, "controllers.Application", "getPersona", Seq(classOf[Int]),"GET", """""", Routes.prefix + """persona/$Id<[^/]+>"""))
   }
}
        

// @LINE:17
case controllers_Application_deletePersona7(params) => {
   call(params.fromPath[Int]("Id", None)) { (Id) =>
        invokeHandler(controllers.Application.deletePersona(Id), HandlerDef(this, "controllers.Application", "deletePersona", Seq(classOf[Int]),"DELETE", """""", Routes.prefix + """persona/$Id<[^/]+>"""))
   }
}
        

// @LINE:18
case controllers_Application_createPersona8(params) => {
   call { 
        invokeHandler(controllers.Application.createPersona(), HandlerDef(this, "controllers.Application", "createPersona", Nil,"POST", """""", Routes.prefix + """persona/"""))
   }
}
        

// @LINE:19
case controllers_Application_updatePersona9(params) => {
   call(params.fromPath[Int]("Id", None)) { (Id) =>
        invokeHandler(controllers.Application.updatePersona(Id), HandlerDef(this, "controllers.Application", "updatePersona", Seq(classOf[Int]),"PUT", """""", Routes.prefix + """persona/$Id<[^/]+>"""))
   }
}
        
}

}
     