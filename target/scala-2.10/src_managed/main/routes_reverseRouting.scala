// @SOURCE:/home/gdmc/Documents/play/play2_demo/conf/routes
// @HASH:77b45a56784b4b046236846060a120bc9a80834a
// @DATE:Sun Jan 22 23:52:22 VET 2017

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString


// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:9
// @LINE:8
// @LINE:5
package controllers {

// @LINE:9
class ReverseWebJarAssets {
    

// @LINE:9
def at(file:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "webjars/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                
    
}
                          

// @LINE:8
class ReverseAssets {
    

// @LINE:8
def at(file:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                
    
}
                          

// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:5
class ReverseApplication {
    

// @LINE:14
def eliminarPersona(Id:Int): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "eliminarPersona/" + implicitly[PathBindable[Int]].unbind("Id", Id))
}
                                                

// @LINE:16
def getPersona(Id:Int): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "persona/" + implicitly[PathBindable[Int]].unbind("Id", Id))
}
                                                

// @LINE:17
def deletePersona(Id:Int): Call = {
   Call("DELETE", _prefix + { _defaultPrefix } + "persona/" + implicitly[PathBindable[Int]].unbind("Id", Id))
}
                                                

// @LINE:12
def listarPersonas(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "personas")
}
                                                

// @LINE:13
def nuevaPersona(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "nuevaPersona")
}
                                                

// @LINE:5
def index(): Call = {
   Call("GET", _prefix)
}
                                                

// @LINE:18
def createPersona(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "persona/")
}
                                                

// @LINE:19
def updatePersona(Id:Int): Call = {
   Call("PUT", _prefix + { _defaultPrefix } + "persona/" + implicitly[PathBindable[Int]].unbind("Id", Id))
}
                                                
    
}
                          
}
                  


// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:9
// @LINE:8
// @LINE:5
package controllers.javascript {

// @LINE:9
class ReverseWebJarAssets {
    

// @LINE:9
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.WebJarAssets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "webjars/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        
    
}
              

// @LINE:8
class ReverseAssets {
    

// @LINE:8
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        
    
}
              

// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:5
class ReverseApplication {
    

// @LINE:14
def eliminarPersona : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.eliminarPersona",
   """
      function(Id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "eliminarPersona/" + (""" + implicitly[PathBindable[Int]].javascriptUnbind + """)("Id", Id)})
      }
   """
)
                        

// @LINE:16
def getPersona : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.getPersona",
   """
      function(Id) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "persona/" + (""" + implicitly[PathBindable[Int]].javascriptUnbind + """)("Id", Id)})
      }
   """
)
                        

// @LINE:17
def deletePersona : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.deletePersona",
   """
      function(Id) {
      return _wA({method:"DELETE", url:"""" + _prefix + { _defaultPrefix } + """" + "persona/" + (""" + implicitly[PathBindable[Int]].javascriptUnbind + """)("Id", Id)})
      }
   """
)
                        

// @LINE:12
def listarPersonas : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.listarPersonas",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "personas"})
      }
   """
)
                        

// @LINE:13
def nuevaPersona : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.nuevaPersona",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "nuevaPersona"})
      }
   """
)
                        

// @LINE:5
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        

// @LINE:18
def createPersona : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.createPersona",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "persona/"})
      }
   """
)
                        

// @LINE:19
def updatePersona : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.updatePersona",
   """
      function(Id) {
      return _wA({method:"PUT", url:"""" + _prefix + { _defaultPrefix } + """" + "persona/" + (""" + implicitly[PathBindable[Int]].javascriptUnbind + """)("Id", Id)})
      }
   """
)
                        
    
}
              
}
        


// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:9
// @LINE:8
// @LINE:5
package controllers.ref {


// @LINE:9
class ReverseWebJarAssets {
    

// @LINE:9
def at(file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.WebJarAssets.at(file), HandlerDef(this, "controllers.WebJarAssets", "at", Seq(classOf[String]), "GET", """""", _prefix + """webjars/$file<.+>""")
)
                      
    
}
                          

// @LINE:8
class ReverseAssets {
    

// @LINE:8
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      
    
}
                          

// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:5
class ReverseApplication {
    

// @LINE:14
def eliminarPersona(Id:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.eliminarPersona(Id), HandlerDef(this, "controllers.Application", "eliminarPersona", Seq(classOf[Int]), "GET", """""", _prefix + """eliminarPersona/$Id<[^/]+>""")
)
                      

// @LINE:16
def getPersona(Id:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.getPersona(Id), HandlerDef(this, "controllers.Application", "getPersona", Seq(classOf[Int]), "GET", """""", _prefix + """persona/$Id<[^/]+>""")
)
                      

// @LINE:17
def deletePersona(Id:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.deletePersona(Id), HandlerDef(this, "controllers.Application", "deletePersona", Seq(classOf[Int]), "DELETE", """""", _prefix + """persona/$Id<[^/]+>""")
)
                      

// @LINE:12
def listarPersonas(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.listarPersonas(), HandlerDef(this, "controllers.Application", "listarPersonas", Seq(), "GET", """""", _prefix + """personas""")
)
                      

// @LINE:13
def nuevaPersona(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.nuevaPersona(), HandlerDef(this, "controllers.Application", "nuevaPersona", Seq(), "GET", """""", _prefix + """nuevaPersona""")
)
                      

// @LINE:5
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Seq(), "GET", """""", _prefix + """""")
)
                      

// @LINE:18
def createPersona(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.createPersona(), HandlerDef(this, "controllers.Application", "createPersona", Seq(), "POST", """""", _prefix + """persona/""")
)
                      

// @LINE:19
def updatePersona(Id:Int): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.updatePersona(Id), HandlerDef(this, "controllers.Application", "updatePersona", Seq(classOf[Int]), "PUT", """""", _prefix + """persona/$Id<[^/]+>""")
)
                      
    
}
                          
}
        
    