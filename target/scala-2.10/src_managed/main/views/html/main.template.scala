
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object main extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[String,Html,play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(title: String)(content: Html):play.api.templates.HtmlFormat.Appendable = {
        _display_ {

Seq[Any](format.raw/*1.32*/("""

<!DOCTYPE html>
<html>
    <head>
        <title>"""),_display_(Seq[Any](/*6.17*/title)),format.raw/*6.22*/("""</title>
        <link rel='shortcut icon' type='image/png' href='"""),_display_(Seq[Any](/*7.59*/routes/*7.65*/.Assets.at("images/favicon.png"))),format.raw/*7.97*/("""'>
        <link rel='stylesheet' href='"""),_display_(Seq[Any](/*8.39*/routes/*8.45*/.WebJarAssets.at(WebJarAssets.locate("bootstrap.min.css")))),format.raw/*8.103*/("""'>
        <script type='text/javascript' src='"""),_display_(Seq[Any](/*9.46*/routes/*9.52*/.WebJarAssets.at(WebJarAssets.locate("jquery.min.js")))),format.raw/*9.106*/("""'></script>
        <script type='text/javascript' src='"""),_display_(Seq[Any](/*10.46*/routes/*10.52*/.Assets.at("js/paginator/js/jquery.quick.pagination.min.js"))),format.raw/*10.112*/("""'></script>
        <link rel="stylesheet" type="text/css" href='"""),_display_(Seq[Any](/*11.55*/routes/*11.61*/.Assets.at("js/paginator/css/styles.css"))),format.raw/*11.102*/("""'>
        <link rel="stylesheet" type="text/css" href='"""),_display_(Seq[Any](/*12.55*/routes/*12.61*/.Assets.at("css/test.css"))),format.raw/*12.87*/("""'>
        <style>
        body """),format.raw/*14.14*/("""{"""),format.raw/*14.15*/("""
            margin-top: 50px;
        """),format.raw/*16.9*/("""}"""),format.raw/*16.10*/("""
        </style>

        <script>

                $(document).ready(function () """),format.raw/*21.47*/("""{"""),format.raw/*21.48*/("""
                    $("ul.pagination1").quickPagination("""),format.raw/*22.57*/("""{"""),format.raw/*22.58*/("""pagerLocation:"both",pageSize:"4""""),format.raw/*22.91*/("""}"""),format.raw/*22.92*/(""");

                """),format.raw/*24.17*/("""}"""),format.raw/*24.18*/(""");
        </script>

    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a id="titleLink" class="brand" href="/">"""),_display_(Seq[Any](/*32.63*/title)),format.raw/*32.68*/("""</a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
        """),_display_(Seq[Any](/*37.10*/content)),format.raw/*37.17*/("""
        </div>
    </body>
</html>
"""))}
    }
    
    def render(title:String,content:Html): play.api.templates.HtmlFormat.Appendable = apply(title)(content)
    
    def f:((String) => (Html) => play.api.templates.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Sun Jan 22 14:38:45 VET 2017
                    SOURCE: /home/gdmc/Documents/play/play2_demo/app/views/main.scala.html
                    HASH: ed5484dab6c7c93cbd3769da4a0bec2b4b7a1771
                    MATRIX: 778->1|902->31|989->83|1015->88|1117->155|1131->161|1184->193|1260->234|1274->240|1354->298|1437->346|1451->352|1527->406|1620->463|1635->469|1718->529|1820->595|1835->601|1899->642|1992->699|2007->705|2055->731|2115->763|2144->764|2210->803|2239->804|2350->887|2379->888|2464->945|2493->946|2554->979|2583->980|2631->1000|2660->1001|2934->1239|2961->1244|3106->1353|3135->1360
                    LINES: 26->1|29->1|34->6|34->6|35->7|35->7|35->7|36->8|36->8|36->8|37->9|37->9|37->9|38->10|38->10|38->10|39->11|39->11|39->11|40->12|40->12|40->12|42->14|42->14|44->16|44->16|49->21|49->21|50->22|50->22|50->22|50->22|52->24|52->24|60->32|60->32|65->37|65->37
                    -- GENERATED --
                */
            