
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object index extends BaseScalaTemplate[play.api.templates.HtmlFormat.Appendable,Format[play.api.templates.HtmlFormat.Appendable]](play.api.templates.HtmlFormat) with play.api.templates.Template2[java.util.List[Persona],Form[Persona],play.api.templates.HtmlFormat.Appendable] {

    /**/
    def apply/*1.2*/(personas: java.util.List[Persona], personaForm: Form[Persona]):play.api.templates.HtmlFormat.Appendable = {
        _display_ {import helper._

import play.data.Form;


Seq[Any](format.raw/*1.65*/("""

"""),format.raw/*5.1*/("""
"""),_display_(Seq[Any](/*6.2*/main("Menu Personas")/*6.23*/ {_display_(Seq[Any](format.raw/*6.25*/("""

    <div class="well">
        <h1>"""),_display_(Seq[Any](/*9.14*/personas/*9.22*/.size)),format.raw/*9.27*/(""" personas registradas.</h1>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <input type="text" name="IdBuscar" id="IdBuscar" value="">
            <button onclick="buscar()">Buscar</button>

            <div id="busqueda">
                DNI:&nbsp;<span id="dniTxt"> </span>
                    &nbsp;Nombre: <span id="nombreTxt"></span>
                    &nbsp;Tel: <span id="telefonoTxt"></span>
                    &nbsp;Mail: <span id="emailTxt"></span>
                    <br>
                    <span id="nre"> </span>
            </div>


        </div>

        <script>

                function buscar() """),format.raw/*31.35*/("""{"""),format.raw/*31.36*/("""
                    $('#busqueda span').text("");

                    var id = $('#IdBuscar').val();

                    $.ajax("""),format.raw/*36.28*/("""{"""),format.raw/*36.29*/("""
                        url: 'http://localhost:9000/persona/' + id,
                        dataType: 'text',
                        method: "GET",
                        success: function (response) """),format.raw/*40.54*/("""{"""),format.raw/*40.55*/("""
                            data = JSON.parse(response);
                            $('#dniTxt').text(data.dni);
                            $('#nombreTxt').text(data.nombre + " " + data.apellido);
                            $('#telefonoTxt').text(data.telefono);
                            $('#emailTxt').text(data.email);
                        """),format.raw/*46.25*/("""}"""),format.raw/*46.26*/(""",
                        error: function (et) """),format.raw/*47.46*/("""{"""),format.raw/*47.47*/("""
                            $('#nre').text("No hay resultados");
                        """),format.raw/*49.25*/("""}"""),format.raw/*49.26*/("""
                    """),format.raw/*50.21*/("""}"""),format.raw/*50.22*/(""");
                """),format.raw/*51.17*/("""}"""),format.raw/*51.18*/("""
        </script>


    </div>


    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6">
            <ul class="pagination1">
            """),_display_(Seq[Any](/*61.14*/for(persona <- personas) yield /*61.38*/ {_display_(Seq[Any](format.raw/*61.40*/("""
                <li>
                    """),_display_(Seq[Any](/*63.22*/form(routes.Application.eliminarPersona(persona.id))/*63.74*/ {_display_(Seq[Any](format.raw/*63.76*/("""
                        <h5>"""),_display_(Seq[Any](/*64.30*/persona/*64.37*/.dni)),format.raw/*64.41*/(""" """),_display_(Seq[Any](/*64.43*/persona/*64.50*/.nombre)),format.raw/*64.57*/(""" """),_display_(Seq[Any](/*64.59*/persona/*64.66*/.apellido)),format.raw/*64.75*/(""" """),_display_(Seq[Any](/*64.77*/persona/*64.84*/.telefono)),format.raw/*64.93*/(""" """),_display_(Seq[Any](/*64.95*/persona/*64.102*/.email)),format.raw/*64.108*/("""
                            <input type="submit" value="Eliminar" class="pull-right">
                            """)))})),format.raw/*66.30*/("""
                        </h5>
                </li>

            """)))})),format.raw/*70.14*/("""
            </ul>

        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <h2>Agregue una persona</h2>

            """),_display_(Seq[Any](/*77.14*/form(routes.Application.nuevaPersona())/*77.53*/ {_display_(Seq[Any](format.raw/*77.55*/("""

                """),_display_(Seq[Any](/*79.18*/inputText(personaForm("dni")))),format.raw/*79.47*/("""
                """),_display_(Seq[Any](/*80.18*/inputText(personaForm("nombre")))),format.raw/*80.50*/("""
                """),_display_(Seq[Any](/*81.18*/inputText(personaForm("apellido")))),format.raw/*81.52*/("""
                """),_display_(Seq[Any](/*82.18*/inputText(personaForm("telefono")))),format.raw/*82.52*/("""
                """),_display_(Seq[Any](/*83.18*/inputText(personaForm("email")))),format.raw/*83.49*/("""

                <input type="submit" value="Crear">

                """)))})),format.raw/*87.18*/("""

        </div>
    </div>
    <div class="row">


    </div>

""")))})),format.raw/*96.2*/("""
"""))}
    }
    
    def render(personas:java.util.List[Persona],personaForm:Form[Persona]): play.api.templates.HtmlFormat.Appendable = apply(personas,personaForm)
    
    def f:((java.util.List[Persona],Form[Persona]) => play.api.templates.HtmlFormat.Appendable) = (personas,personaForm) => apply(personas,personaForm)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Mon Jan 23 00:45:12 VET 2017
                    SOURCE: /home/gdmc/Documents/play/play2_demo/app/views/index.scala.html
                    HASH: 26edd0b2fa1d22789d723ef5d45c5fa49367c7ce
                    MATRIX: 805->1|1002->64|1030->107|1066->109|1095->130|1134->132|1207->170|1223->178|1249->183|1946->852|1975->853|2134->984|2163->985|2394->1188|2423->1189|2803->1541|2832->1542|2907->1589|2936->1590|3054->1680|3083->1681|3132->1702|3161->1703|3208->1722|3237->1723|3428->1878|3468->1902|3508->1904|3587->1947|3648->1999|3688->2001|3754->2031|3770->2038|3796->2042|3834->2044|3850->2051|3879->2058|3917->2060|3933->2067|3964->2076|4002->2078|4018->2085|4049->2094|4087->2096|4104->2103|4133->2109|4281->2225|4380->2292|4555->2431|4603->2470|4643->2472|4698->2491|4749->2520|4803->2538|4857->2570|4911->2588|4967->2622|5021->2640|5077->2674|5131->2692|5184->2723|5288->2795|5384->2860
                    LINES: 26->1|32->1|34->5|35->6|35->6|35->6|38->9|38->9|38->9|60->31|60->31|65->36|65->36|69->40|69->40|75->46|75->46|76->47|76->47|78->49|78->49|79->50|79->50|80->51|80->51|90->61|90->61|90->61|92->63|92->63|92->63|93->64|93->64|93->64|93->64|93->64|93->64|93->64|93->64|93->64|93->64|93->64|93->64|93->64|93->64|93->64|95->66|99->70|106->77|106->77|106->77|108->79|108->79|109->80|109->80|110->81|110->81|111->82|111->82|112->83|112->83|116->87|125->96
                    -- GENERATED --
                */
            