# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table persona (
  id                        integer auto_increment not null,
  dni                       varchar(255),
  nombre                    varchar(255),
  apellido                  varchar(255),
  telefono                  varchar(255),
  email                     varchar(255),
  constraint pk_persona primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table persona;

SET FOREIGN_KEY_CHECKS=1;

