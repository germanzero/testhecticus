package models;

import java.util.*;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.api.libs.json.JsValue;
import play.api.libs.json.Writes;
import play.db.ebean.*;
import play.libs.Json;
import play.mvc.BodyParser;
import scala.Function1;

import javax.persistence.*;



/**
 * Created by gdmc on 1/21/17.
 */



@Entity
public class Persona extends Model implements Writes<Persona> {

    @Id
    public Integer id;

    public String dni;
    public String nombre;
    public String apellido;
    public String telefono;
    public String email;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static Finder<Integer,Persona> find = new Finder(Integer.class, Persona.class);

    public static List<Persona> all() {
        return find.all();
    }

    public static void create(Persona persona){
        persona.save();
    }

    public static void delete(Integer id){
        find.ref(id).delete();
    }

    public static Persona findById(Integer id){

        return find.byId(id);

    }

    public static ObjectNode personToJson(Persona persona){

        ObjectNode result = Json.newObject();

        result.put("dni", persona.dni);
        result.put("nombre", persona.nombre);
        result.put("apellido", persona.apellido);
        result.put("telefono", persona.telefono);
        result.put("email", persona.email);

        return result;
    }



    @Override
    public JsValue writes(Persona o) {
        return null;
    }

    @Override
    public Writes<Persona> transform(Function1<JsValue, JsValue> transformer) {
        return transform(transformer);
    }

    @Override
    public Writes<Persona> transform(Writes<JsValue> transformer) {
        return transform(transformer);
    }
}
