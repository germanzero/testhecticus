package controllers;

import models.Persona;
import play.api.libs.json.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.data.*;

import javax.jws.WebParam;
import java.io.InputStream;

import static play.mvc.Http.*;


public class Application extends Controller {


    static Form<Persona> personaForm = Form.form(Persona.class);
    static Persona personaPost = new Persona();

    public static Result index() {

        return redirect(routes.Application.listarPersonas());
    }


    public static Result listarPersonas(){
        return ok(
                views.html.index.render(Persona.all(), personaForm)
        );
    }

    public static Result nuevaPersona(){
        Form<Persona> filledForm = personaForm.bindFromRequest();
        if(filledForm.hasErrors()){
            return badRequest(views.html.index.render(Persona.all(),filledForm));

        }else{
            Persona.create(filledForm.get());
            return redirect(routes.Application.listarPersonas());

        }


    }

    public static Result eliminarPersona(Integer Id){
        Persona.delete(Id);
        return redirect(routes.Application.listarPersonas());
    }

    public static Result getPersona(Integer Id){
        Persona persona = Persona.findById(Id);
        return ok(Persona.personToJson(persona).toString());

    }


    //API

    public static Result deletePersona(Integer Id){
        Persona.delete(Id);
        return ok("DELETED");

    }

    public static Result createPersona(){

        personaPost = setPersona();
        personaPost.save();
        return getPersona(personaPost.id);

    }

    public static Result updatePersona(Integer Id){
        personaPost = Persona.findById(Id);
        personaPost = setPersona();
        personaPost.save();
        return getPersona(personaPost.id);

    }


    public static Persona setPersona(){

        personaPost.setDni(request().getQueryString("dni"));
        personaPost.setNombre(request().getQueryString("nombre"));
        personaPost.setApellido(request().getQueryString("apellido"));
        personaPost.setTelefono(request().getQueryString("telefono"));
        personaPost.setEmail(request().getQueryString("email"));

        return personaPost;

    }



}
